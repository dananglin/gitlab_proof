# Gitlab Proof For Keyoxide

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account.

For details check out https://keyoxide.org/guides/openpgp-proofs

My Keyoxide profile: https://keyoxide.org/3A8B819A5E8795D75FDFF360B135D610070D4526
